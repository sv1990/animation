#include "animation.hh"

#include <QPainter>

void animation::paintEvent(QPaintEvent*) {
  QPainter painter(this);
  painter.fillRect(rect(), Qt::black);
  auto pixmapOffset = QPoint();
  painter.drawPixmap(pixmapOffset, _pixmap);
}

void animation::update_frame(const QImage& image) {
  _pixmap = QPixmap::fromImage(image);
  resize(image.width(), image.height());
  update();
}
