#ifndef _ANIMATION_ANIMATION_HH_1528286932659697981_
#define _ANIMATION_ANIMATION_HH_1528286932659697981_

#include "worker_facade.hh"

#include <QWidget>

class animation : public QWidget {
  worker_facade& _worker;
  QPixmap _pixmap;

public:
  explicit animation(worker_facade& worker, QWidget* parent = nullptr)
      : QWidget(parent), _worker(worker) {
    connect(&_worker, &worker_facade::rendered_image, this,
            &animation::update_frame);
    _worker.start();
  }

protected:
  virtual void paintEvent(QPaintEvent* event) override;

private slots:
  virtual void update_frame(const QImage& image);
};

#endif // _ANIMATION_ANIMATION_HH_1528286932659697981_
