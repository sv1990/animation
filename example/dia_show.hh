#ifndef ANIMATION3_DIA_SHOW_HH_1528268596230317305_
#define ANIMATION3_DIA_SHOW_HH_1528268596230317305_

#include "worker_facade.hh"

#include <QImage>

#include <algorithm>
#include <filesystem>
#include <string>
#include <vector>

#include <cctype>

namespace fs = std::filesystem;

namespace {
/**
 * Helper function to load the images
 *
 */
std::vector<std::string> get_images(const std::string& dir) noexcept {
  std::vector<std::string> images;
  auto dir_iter = fs::directory_iterator{dir};
  std::transform(begin(dir_iter), end(dir_iter), std::back_inserter(images),
                 [](const auto& path) { return fs::canonical(path).string(); });
  std::sort(begin(images), end(images), [](const auto& l, const auto& r) {
    return std::stoul(fs::path{l}.stem()) < std::stoul(fs::path{r}.stem());
  });
  return images;
}
} // namespace

class dia_show : public worker_facade {
  std::vector<std::string> _images;
  std::vector<std::string>::iterator _curr;

public:
  explicit dia_show(const std::string& dir)
      : _images(get_images(dir)), _curr(begin(_images)) {}
  virtual QImage next() override {
    return QImage{QString::fromStdString(*_curr++)};
  }
  virtual bool done() override { return _curr == end(_images); }
  virtual void pause() override { msleep(500); }
};

#endif // ANIMATION3_DIA_SHOW_HH_1528268596230317305_
