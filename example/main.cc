#include "animation.hh"
#include "dia_show.hh"

#include <QApplication>

int main(int argc, char** argv) {
  QApplication app(argc, argv);
  dia_show ds("example/images");
  animation a(ds);
  a.show();
  return app.exec();
}
