#include "worker_facade.hh"

void worker_facade::run() {
  while (!done()) {
    emit rendered_image(next());
    pause();
  }
}
