#ifndef ANIMATION3_WORKER_FACADE_HH_1528268493685047157_
#define ANIMATION3_WORKER_FACADE_HH_1528268493685047157_

#include <QImage>
#include <QThread>

class worker_facade : public QThread {
  Q_OBJECT
public:
  worker_facade() = default;

  /**
   * Return the next image as a QImage object
   *
   */
  virtual QImage next() = 0;
  /**
   * Notify that there are no more images left
   *
   */
  virtual bool done() { return false; }
  /**
   * Pause the imaging loop if necessary. Will probably use one of
   * QThread::(sleep|msleep|usleep)
   */
  virtual void pause() {}
  virtual ~worker_facade() {}

protected:
  /**
   * Implements the imaging loop and emits the rendered_image signal
   */
  virtual void run() override;

signals:
  void rendered_image(const QImage& image);
};

#endif // ANIMATION3_WORKER_FACADE_HH_1528268493685047157_
